import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'Flutter Basic List Demo';

    return MaterialApp(
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          title: Text(appTitle),
        ),
        body: ListView(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.map),
              title: Text('Map'),
              onTap: () {
                Fluttertoast.showToast(
                  msg: 'Map selected',
                  backgroundColor: Colors.grey,
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.photo_album),
              title: Text('Album'),
              onTap: () {
                Fluttertoast.showToast(
                  msg: 'Album selected',
                  backgroundColor: Colors.grey,
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.phone),
              title: Text('Phone'),
              onTap: () {
                Fluttertoast.showToast(
                  msg: 'Phone selected',
                  backgroundColor: Colors.grey,
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.contacts),
              title: Text('Contact'),
              onTap: () {
                Fluttertoast.showToast(
                  msg: 'Contact selected',
                  backgroundColor: Colors.grey,
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text('Setting'),
              onTap: () {
                Fluttertoast.showToast(
                  msg: 'Setting selected',
                  backgroundColor: Colors.grey,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
